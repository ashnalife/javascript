import crypto from "crypto-browserify";
/**
 * 
 * @param {string} key 
 * @param {string} iv 
 * @param {string} data 
 * @returns {string}
 * @description encrypt data by using key and vector by using block cypher aes-128-cbc techique
 */
function encrypt(key, iv, data) {
  const cipher = crypto.createCipheriv("aes-128-cbc", key, iv);
  let encrypted = cipher.update(data, "utf8", "base64");
  encrypted += cipher.final("base64");
  return encrypted;
}

/**
 * 
 * @param {string} key 
 * @param {string} iv 
 * @param {string} encryptedData //aes-128-cbc encrypted string for compatibility
 * @returns {string}
 * @description decrypt encrypted string based on aes-128-cbc
 */
function decrypt(key, iv, encryptedData) {
  const encrypted = Buffer.from(encryptedData, "base64");
  const decipher = crypto.createDecipheriv("aes-128-cbc", key, iv);
  let decrypted = decipher.update(encrypted, "binary", "utf8");
  decrypted += decipher.final("utf8");
  return decrypted;
}

function App() {
  const key = "EncryptionKey";
  const iv = "InitialVector";
  const data1 = "SomeData";
  const data2 = "SomeOtherData";

  const encrypted1 = encrypt(key, iv, data1);
  const decrypted1 = decrypt(key, iv, encrypted1);

  const encrypted2 = encrypt(key, iv, data2);
  const decrypted2 = decrypt(key, iv, encrypted2);

  const strGot =
    "http://localhost:8080/cart/tracking?orderId=qm50udXFwEo7zRV2veT6Pg%3D%3D&shipBinId=pBwNjQSVnx9eXG2no%2B2knw%3D%3D&trackingId=EZ4000000004&shipper=UPS&deliveryMethod=MailOrder&source=web#xd_co_f=NzIyNjFkYWYtN2ViNS00YTM4LTlkZTYtZjVhODg1NDU1OGIz~";
  const strHere = `http://localhost:8080/cart/tracking?orderId=${encrypted1}&shipBinId=${encrypted2}&trackingId=EZ3000000003&shipper=UPS&deliveryMethod=MailOrder&source=web`;
  return (
    <div>
      <p>data data: {data1}</p>
      <p>Encrypted data: {encrypted1}</p>
      <p>decrypted data: {decrypted1}</p>
      <br />
      <p>data data: {data2}</p>
      <p>Encrypted data: {encrypted2}</p>
      <p>decrypted data: {decrypted2}</p>
      <p>{`http://localhost:8080/cart/tracking?orderId=${data1}&shipBinId=${data2}&trackingId=EZ3000000003&shipper=UPS&deliveryMethod=MailOrder&source=web`}</p>
      <p>{strGot === strHere ? "same strings" : "different strings"}</p>
    </div>
  );
}

export default App;
