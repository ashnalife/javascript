const MailBuilder = (function() {
    function MailBuilder(id) {
        //instance section
        this.id = id;
    }

    /** @private */
    let privateMethod = function() {

    }
    //static method
    MailBuilder.build = function() {
        console.log("bind called");
    }

//inserting method into function prototype.
    MailBuilder.prototype.createAddress = function(address) {
        this.address = address;
        console.log("createAddress called");
    }
    return MailBuilder;
})();



/** @class @extends {MailBuilder} */
const ChildMailBuilder = (function() {
    function ChildMailBuilder(id, name) {
        this.id = id;
        this.childName = name;
    }
//-------------Key logic start------------------>
		//all property and methods of parent class should be visible in child with ovverriding..
    ChildMailBuilder = Object.assign(MailBuilder, ChildMailBuilder);
		//all property and methods parent prototype should also present in child with overrding
    ChildMailBuilder.prototype = Object.assign(MailBuilder.prototype, ChildMailBuilder.prototype);
//---------------key logic end----------------->

    ChildMailBuilder.prototype.createSupara = function(s) {
        this.s = s;
        console.log("createSupara called");
    }
    return ChildMailBuilder;
})();

ChildMailBuilder.build();
//new ChildMailBuilder('id', 'randomstr').createSupara('address');
new ChildMailBuilder('id', 'randomstr').createAddress('address');