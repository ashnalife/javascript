//promise is a place holder value
// unlike subscribing through call-back we immidiately collect a placeholder
// promise unsettled (state:-pending) promise settled( state:- fullfilled or rejected)

// promiseRef.then(fulfillment handler,rejection handler)

//Any object that implements the then() method in this way is called a thenable. All promises
//are thenables, but not all thenables are promises.

//Promises also have a catch() method that behaves the same as then() when only a rejection handler
//is passed.

// Creating Settled Promises (fulfilled and unfullfilled)
const spf = Promise.resolve(45);
//const spuf = Promise.reject('reason of rejection');



// Creation of pending/unsettled promise

const resolver = (resolve, reject) => {
  setTimeout(() => {
    if (new Date().getTime() % 2 === 0) {
      resolve(2);
    }
    reject("no reason for rejection.");
  }, 2000);
};

const pp = new Promise(resolver);
pp.then((response) => {
  console.log(response);
});
pp.catch((reason) => {
  console.log(reason);
});


//Promise.All Polyfill.
/**
 * 
 * @param {[Promise]} promiseList 
 * @returns {Promise}
 */
const promiseAll = (promiseList) => {
    return new Promise((resolve,reject)=>{
        let count = promiseList.length;//This flag is to keep track ie array.map is synchronous.
        const resultList = [];
        promiseList.forEach((promise,index)=>{
            promise.then((response)=>{
                count--;
                resultList.push(response);
                if(count === 0){
                    resolve(resultList); //if all element parsed and not burst yet we can resolve.
                }
            }).catch(reason=>{
                reject(`${index} promise got rejected ${reason}`);
            });
        });
    });
};

//Promise.All.Settled Polyfill.
/**
 * 
 * @param {[Promise]} promiseList 
 * @returns {Promise}
 */
const promiseAllSettled = (promiseList) => {
    return new Promise((resolve,reject)=>{
        let count = promiseList.length;//This flag is to keep track ie array.map is synchronous.
        const resultList = [];
        promiseList.forEach((promise,index)=>{
            promise.then((response)=>{
                count--;
                resultList.push(response);
                if(count === 0){
                    resolve(resultList); //if all element parsed and not burst yet we can resolve.
                }
            }).catch(reason=>{
                count--;
                resultList.push(reason);
                if(count === 0){
                    resolve(resultList); //if all element parsed and not burst yet we can resolve.
                }
            });
        });
    });
};

const spfArray = [9, 8, 7, 6, 5, 4, 3, 2, 1].map((number) =>
  Promise.resolve(number)
);
//spfArray.push(Promise.reject('test'));

console.log(promiseAllSettled(spfArray));
