/**
 *
 * @param {string} url
 * @returns {Promise<any>}
 */
const fetcherGET = async (url) => {
  try {
    const respose = await fetch(url);
    return await respose.json();
  } catch (error) {
    return error;
  }
};

const GET_JOBID_LIST = "https://hacker-news.firebaseio.com/v0/jobstories.json";

/**
 *
 * @returns {Promise<{jobId: {title: string}}>}
 */
const fetchAllJobsById = async () => {
  try {
    let jobIDList = await fetcherGET(GET_JOBID_LIST);
    let jobDescriptionList = [];
    if (Array.isArray(jobIDList)) {
      jobDescriptionList = await Promise.allSettled(
        jobIDList.map((jobId) =>
          fetcherGET(`https://hacker-news.firebaseio.com/v0/item/${jobId}.json`)
        )
      );
    } else {
      throw new Error("receaved invalid respone from API /jobstories.json");
    }
    return jobDescriptionList
      .filter((settledObject) => settledObject.status === "fulfilled")
      .map((fullfilledPromises) => fullfilledPromises.value);
  } catch (error) {
    console.error(error);
  }
};
//Invocation.
fetchAllJobsById().then((jobDescList) => {
  console.log(jobDescList);
});
